import tensorflow as tf
AUTOTUNE = tf.data.experimental.AUTOTUNE
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Conv2D, Conv2DTranspose
from keras.constraints import max_norm

BATCH_SIZE = 300
IMG_HEIGHT = 300
IMG_WIDTH = 300

def add_noise(images, count):
  # Add noise to the input image
  for i in range(0,count):
    images[i] = images[i] + 1 * images[i].std() * np.random.random(images[i].shape)
  return images
def show_batch(image_batch):
  plt.figure(figsize=(40, 40))
  image_batch = image_batch
  plt.imshow(image_batch)
  plt.axis('off')
  plt.show()
def decode_img(img):
  # convert the compressed string to a 3D uint8 tensor
  img = tf.image.decode_jpeg(img, channels=3)
  # Use `convert_image_dtype` to convert to floats in the [0,1] range.
  img = tf.image.convert_image_dtype(img, tf.float32)
  # resize the image to the desired size.
  return tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])

def process_path(file_path):
  # label = get_label(file_path)
  # load the raw data from the file as a string
  img = tf.io.read_file(file_path)
  img = decode_img(img)
  return img
def prepare_for_training(ds, cache=True, shuffle_buffer_size=1000):
  # This is a small dataset, only load it once, and keep it in memory.
  # use `.cache(filename)` to cache preprocessing work for datasets that don't
  # fit in memory.
  if cache:
    if isinstance(cache, str):
      ds = ds.cache(cache)
    else:
      ds = ds.cache()

  ds = ds.shuffle(buffer_size=shuffle_buffer_size)

  # Repeat forever
  ds = ds.repeat()

  ds = ds.batch(BATCH_SIZE)

  # `prefetch` lets the dataset fetch batches in the background while the model
  # is training.
  ds = ds.prefetch(buffer_size=AUTOTUNE)

  return ds
list_ds = tf.data.Dataset.list_files("images/*.JPG")
count = 0
for f in list_ds:
  count = count + 1
  # print(f.numpy())
print(count)

labeled_ds = list_ds.map(process_path, tf.data.experimental.AUTOTUNE)
for image in labeled_ds.take(1):
  print("Image shape: ", image.numpy().shape)

train_ds = prepare_for_training(labeled_ds)
print(train_ds)
image_batch = next(iter(train_ds))
print(image_batch.numpy().shape)
show_batch(image_batch[0].numpy())

# Model configuration
img_width, img_height = 300, 300
batch_size = 300
no_epochs = 50
validation_split = 0.2100
verbosity = 1
max_norm_value = 2.0
noise_factor = 0.55
number_of_visualizations = 6

input_shape = (img_width, img_height, 3)
# image_batch = image_batch / 255
# Add noise
pure = image_batch.numpy()
noisy_input = add_noise(image_batch.numpy(), count)
# Add noise
# noise = np.random.normal(0, 1, pure.shape)
# noisy_input = pure + noise_factor * noise
show_batch(noisy_input[0])

print(pure.shape, noisy_input.shape)
# Create the model
model = Sequential()
model.add(Conv2D(64, kernel_size=(3, 3), kernel_constraint=max_norm(max_norm_value), activation='relu', kernel_initializer='he_uniform', input_shape=input_shape))
model.add(Conv2D(32, kernel_size=(3, 3), kernel_constraint=max_norm(max_norm_value), activation='relu', kernel_initializer='he_uniform'))
model.add(Conv2DTranspose(64, kernel_size=(3,3), kernel_constraint=max_norm(max_norm_value), activation='relu', kernel_initializer='he_uniform'))
model.add(Conv2DTranspose(32, kernel_size=(3,3), kernel_constraint=max_norm(max_norm_value), activation='relu', kernel_initializer='he_uniform'))
model.add(Conv2D(3, kernel_size=(3, 3), kernel_constraint=max_norm(max_norm_value), activation='sigmoid', padding='same'))

model.summary()


# Compile and fit data
model.compile(optimizer='adam', loss='binary_crossentropy')
model.fit(noisy_input, pure,
                epochs=no_epochs,
                batch_size=batch_size,
                validation_split=validation_split)
import cv2
result = model.predict(noisy_input)

show_batch(result[0])
