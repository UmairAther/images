# load and evaluate a saved model
from numpy import loadtxt
from keras.models import load_model
import numpy as np
import cv2
from PIL import Image
import os
import glob
import tensorflow as tf
AUTOTUNE = tf.data.experimental.AUTOTUNE
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Conv2D, Conv2DTranspose
from keras.constraints import max_norm

def crop(im, height, width):
    # im = Image.open(infile)
    imgwidth, imgheight = im.size
    rows = np.int(imgheight/height)
    cols = np.int(imgwidth/width)
    for i in range(rows):
        for j in range(cols):
            # print (i,j)
            box = (j*width, i*height, (j+1)*width, (i+1)*height)
            yield im.crop(box)
import numpy as np
from scipy.ndimage import zoom


def clipped_zoom(img, zoom_factor, **kwargs):

    h, w = img.shape[:2]

    # For multichannel images we don't want to apply the zoom factor to the RGB
    # dimension, so instead we create a tuple of zoom factors, one per array
    # dimension, with 1's for any trailing dimensions after the width and height.
    zoom_tuple = (zoom_factor,) * 2 + (1,) * (img.ndim - 2)

    # Zooming out
    if zoom_factor < 1:

        # Bounding box of the zoomed-out image within the output array
        zh = int(np.round(h * zoom_factor))
        zw = int(np.round(w * zoom_factor))
        top = (h - zh) // 2
        left = (w - zw) // 2

        # Zero-padding
        out = np.zeros_like(img)
        out[top:top+zh, left:left+zw] = zoom(img, zoom_tuple, **kwargs)

    # Zooming in
    elif zoom_factor > 1:

        # Bounding box of the zoomed-in region within the input array
        zh = int(np.round(h / zoom_factor))
        zw = int(np.round(w / zoom_factor))
        top = (h - zh) // 2
        left = (w - zw) // 2

        out = zoom(img[top:top+zh, left:left+zw], zoom_tuple, **kwargs)

        # `out` might still be slightly larger than `img` due to rounding, so
        # trim off any extra pixels at the edges
        trim_top = ((out.shape[0] - h) // 2)
        trim_left = ((out.shape[1] - w) // 2)
        out = out[trim_top:trim_top+h, trim_left:trim_left+w]

    # If zoom_factor == 1, just return the input array
    else:
        out = img
    return out

# change the path and the base name of the image files
imgdir = '.'
basename = '031.JPG'
filelist = glob.glob(os.path.join(imgdir, basename))
for filenum, infile in enumerate(filelist):
    # infile='/Users/alex/Documents/PTV/test_splitter/cal/Camera 1-1-9.tif'
    print(f'file no = {filenum}')  # keep the numbers for the future
    print(f'name is {infile}')
    im = Image.open(infile)
    imgwidth, imgheight = im.size
    print(('Image size is: %d x %d ' % (imgwidth, imgheight)))
    height = np.int(imgheight/2)
    width = np.int(imgwidth/2)
    start_num = 0
    for k, piece in enumerate(crop(im, height, width), start_num):
        # print k
        # print piece
        img=Image.new('RGB', (width,height), 255)
        # print img
        img.paste(piece)
        path = os.path.join("split/cam%d_1%01d.JPG" % (int(k+1),filenum))
        img.save(path)
        os.rename(path, os.path.join("split/cam%d.1%01d.JPG" % (int(k+1), filenum)))

imgs = ['split/cam1.10.JPG', 'split/cam2.10.JPG', 'split/cam3.10.JPG', 'split/cam4.10.JPG']
x = int(input("Enter zoom calaratity 2 or 4:"))
if x != 2 and x != 4:
  print("wrong input zoom function will only work with 2x or 4x computational")
else:
    i = 1
    for img in imgs:
        im = cv2.imread(img)
        zm2 = clipped_zoom(im, x)
        path = "./final/" + str(i) + ".JPG"
        cv2.imwrite(path, zm2)
        i = i + 1
BATCH_SIZE = 4
IMG_HEIGHT = 300
IMG_WIDTH = 300
def show_batch(image_batch):
  plt.figure(figsize=(40, 40))
  image_batch = image_batch
  plt.imshow(image_batch)
  plt.axis('off')
  plt.show()
def decode_img(img):
  # convert the compressed string to a 3D uint8 tensor
  img = tf.image.decode_jpeg(img, channels=3)
  # Use `convert_image_dtype` to convert to floats in the [0,1] range.
  img = tf.image.convert_image_dtype(img, tf.float32)
  # resize the image to the desired size.
  return tf.image.resize(img, [IMG_HEIGHT, IMG_WIDTH])

def process_path(file_path):
  # label = get_label(file_path)
  # load the raw data from the file as a string
  img = tf.io.read_file(file_path)
  img = decode_img(img)
  return img
def prepare_for_training(ds, cache=True, shuffle_buffer_size=1000):
  # This is a small dataset, only load it once, and keep it in memory.
  # use `.cache(filename)` to cache preprocessing work for datasets that don't
  # fit in memory.
  if cache:
    if isinstance(cache, str):
      ds = ds.cache(cache)
    else:
      ds = ds.cache()

  ds = ds.shuffle(buffer_size=shuffle_buffer_size)

  # Repeat forever
  ds = ds.repeat()

  ds = ds.batch(BATCH_SIZE)

  # `prefetch` lets the dataset fetch batches in the background while the model
  # is training.
  ds = ds.prefetch(buffer_size=AUTOTUNE)

  return ds
list_ds = tf.data.Dataset.list_files("final/*.JPG")
count = 0
for f in list_ds:
  count = count + 1
  # print(f.numpy())
print(count)

labeled_ds = list_ds.map(process_path, tf.data.experimental.AUTOTUNE)
for image in labeled_ds.take(1):
  print("Image shape: ", image.numpy().shape)

train_ds = prepare_for_training(labeled_ds)
print(train_ds)
image_batch = next(iter(train_ds))
print(image_batch.numpy().shape)
noise = image_batch.numpy()
# load model
model = load_model('model.h5')
denoised = model.predict(noise)

show_batch(image_batch[0].numpy())
show_batch(image_batch[1].numpy())
show_batch(image_batch[2].numpy())
show_batch(image_batch[3].numpy())
show_batch(denoised[0])
show_batch(denoised[1])
show_batch(denoised[2])
show_batch(denoised[3])

